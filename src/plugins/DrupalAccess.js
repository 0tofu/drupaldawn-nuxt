import axios from 'axios'

const API_BASE_URL = process.env.API_BASE_URL
const API_ENDPOINT_URL = process.env.API_ENDPOINT_URL

export default {
  get_data: async function(path, params = null) {
    const requestUrl = API_BASE_URL + API_ENDPOINT_URL + path
    try {
      const response = await axios.get(requestUrl, { params: params })
      return response.data
    } catch (err) {
      throw new Error('Drupal Access Failed.')
    }
  }
}
