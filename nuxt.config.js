const pkg = require('./package')
const path = require('path')
const webpack = require('webpack')
require('dotenv').config()

module.exports = {
  srcDir: 'src/',
  mode: 'universal',

  /*
  ** Headers of the page
  */
  head: {
    title: pkg.name,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },

  /*
  ** Global CSS
  */
  css: [
    '@/scss/App.scss'
  ],

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [],

  /*
  ** Nuxt.js modules
  */
  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/dotenv',
    'nuxt-material-design-icons',
    [
      'nuxt-sass-resources-loader',
      [
        '@scss/_variables.scss',
        '@@/node_modules/breakpoint-sass/stylesheets/_breakpoint.scss'
      ]
    ]
  ],
  /*
  ** Axios module configuration
  */
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
  },

  env: {
    SITE_DOMAIN: process.env.SITE_DOMAIN,
    API_BASE_URL: process.env.API_BASE_URL,
    API_ENDPOINT_URL: process.env.API_ENDPOINT_URL
  },

  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    },
    plugins: [
      new webpack.ProvidePlugin({
        '_': 'lodash'
      })
    ],
    publicPath: '/assets/'
  },

  generate: {
    interval: 1000,
    async routes() {
      const DrupalAccess = require('./src/plugins/DrupalAccess').default
      const uniq = require('lodash/uniq')
      let routes = []
      let page = 0
      let nextPage = false

      do {
        try {
          const result = await DrupalAccess.get_data('/sites', { page: page })
          if (result) {
            nextPage = result.next
            page += 1
            const sites = result.sites
            const sitesCount = sites.length
            for (let i = 0; i < sitesCount; i++) {
              const site = sites[i]
              const nid = site.id
              routes.push(`/sites/${nid}`)
            }
          } else {
            nextPage = false
          }
        } catch (e) {
          console.log(e)
          nextPage = false
        }
      } while (nextPage)
      routes = uniq(routes)
      return routes
    }
  },

  /*
  ** For IntelliJ.
  */
  resolve: {
    extensions: ['.js', '.json', '.vue', '.ts'],
    root: path.resolve(__dirname),
    alias: {
      '@': path.resolve(__dirname),
      '~': path.resolve(__dirname)
    }
  }
}
